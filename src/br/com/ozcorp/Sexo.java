package br.com.ozcorp;

public enum Sexo {
	MASCULINO("Masculino"), FEMININO("Feminino"), OUTRO("Outro");
	Sexo(String Nome) {
		this.Nome = Nome;
	}
	String Nome;
}
