package br.com.ozcorp;

public class Departamento {
	//Construtor
public Departamento(String nome, String sigla, Cargo cargo) {
		super();
		this.nome = nome;
		this.sigla = sigla;
		this.cargo = cargo;
	}
String nome;
String sigla;
Cargo cargo;
//Getters e stters
public String getNome() {
	return nome;
}
public void setNome(String nome) {
	this.nome = nome;
}
public String getSigla() {
	return sigla;
}
public void setSigla(String sigla) {
	this.sigla = sigla;
}
public Cargo getCargo() {
	return cargo;
}
public void setCargo(Cargo cargo) {
	this.cargo = cargo;
}



}
