package br.com.ozcorp;

public class Funcionario  {
String Nome;
String Rg;
String Cpf;
String Matricula;
String Email;
String Senha;
TipoSanguineo TipoSanguineo;
Sexo Sexo;
int NivelAcesso;
Departamento departamento;
/*
 * Construtor
 */
public Funcionario(String nome, String rg, String cpf, String matricula, String email, String senha,
		TipoSanguineo tipoSanguineo, Sexo sexo, int nivelAcesso, Departamento departamento) {
	
	Nome = nome;
	Rg = rg;
	Cpf = cpf;
	Matricula = matricula;
	Email = email;
	Senha = senha;
	TipoSanguineo = tipoSanguineo;
	Sexo = sexo;
	NivelAcesso = nivelAcesso;
	this.departamento = departamento;
}
/* 
 *Getters e Setters 
 */
//Nome
public String getNome() {
	return Nome;
}
public void setNome(String nome) {
	Nome = nome;
}
//RG
public String getRg() {
	return Rg;
}
public void setRg(String rg) {
	Rg = rg;
}
//CPF
public String getCpf() {
	return Cpf;
}
public void setCpf(String cpf) {
	Cpf = cpf;
}
//Matricula
public String getMatricula() {
	return Matricula;
}
public void setMatricula(String matricula) {
	Matricula = matricula;
}
//Email
public String getEmail() {
	return Email;
}
public void setEmail(String email) {
	Email = email;
}
//Senha
public String getSenha() {
	return Senha;
}
public void setSenha(String senha) {
	Senha = senha;
}
//Tipo Sanguineo
public TipoSanguineo getTipoSanguineo() {
	return TipoSanguineo;
}
public void setTipoSanguineo(TipoSanguineo tipoSanguineo) {
	TipoSanguineo = tipoSanguineo;
}
//Sexo
public Sexo getSexo() {
	return Sexo;
}
public void setSexo(Sexo sexo) {
	Sexo = sexo;
}
//Nivel de Acesso
public int getNivelAcesso() {
	return NivelAcesso;
}
public void setNivelAcesso(int nivelAcesso) {
	NivelAcesso = nivelAcesso;
}
//Departamento
public Departamento getDepartamento() {
	return departamento;
}
public void setDepartamento(Departamento departamento) {
	this.departamento = departamento;
}

public void mostrarDados() {
	System.out.println("Nome: " + this.Nome);
	System.out.println("RG: " + this.Rg);
	System.out.println("CPF: " + this.Cpf);
	System.out.println("matricula: " + this.Matricula);
	System.out.println("Email: " + this.Email);
	System.out.println("Senha: " + this.Senha);
	System.out.println("Departamento: " + this.departamento.nome);
	System.out.println("Sexo: " + this.Sexo.Nome);
	System.out.println("Cargo: " + this.departamento.cargo.titulo);
	System.out.println("Sal�rio: " + this.departamento.cargo.SalarioBase);
	System.out.println("Tipo Sanguineo: " + this.TipoSanguineo);
}

}
